const state = {
  nums: ["0", "0"]
};

const getters = {
  nums: state => state.nums
};

const actions = {
  setNum({ commit }, [num, index]) {
    commit("SET_NUM", num, index);
  }
};

const mutations = {
  SET_NUM(state, [num, index]) {
    state.nums[index] = num;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
